﻿-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1-- Версия сервера: 5.5.27
-- Версия PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `webapp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comment_message`
--

CREATE TABLE IF NOT EXISTS `comment_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `message` varchar(2056) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `comment_message`
--

INSERT INTO `comment_message` (`id`, `user_id`, `parent_id`, `message`, `time_added`) VALUES
(1, 19, NULL, 'First Comment', '2014-05-31 10:22:19'),
(2, 19, NULL, 'Second comment', '2014-05-31 10:25:43'),
(3, 19, 2, NULL, '2014-05-31 11:04:53'),
(4, 19, 3, 'Second Reply', '2014-05-31 11:08:14'),
(5, 19, NULL, NULL, '2014-05-31 11:09:59'),
(6, 19, NULL, 'Four comment', '2014-05-31 11:10:12'),
(7, 19, 5, NULL, '2014-05-31 11:10:25'),
(8, 2, NULL, 'Five comment', '2014-05-31 11:45:21'),
(9, 2, NULL, 'Six comment', '2014-05-31 11:45:36'),
(10, 2, NULL, 'Eight comment', '2014-05-31 11:45:46'),
(11, 2, NULL, 'Nine comment', '2014-05-31 11:45:56'),
(12, 2, NULL, 'Ten comment', '2014-05-31 11:46:07'),
(13, 2, NULL, 'Eleven comment', '2014-05-31 11:46:18'),
(14, 2, NULL, 'Twelve comment', '2014-05-31 11:47:00'),
(15, 2, NULL, 'Thirteen comment', '2014-05-31 11:47:35'),
(16, 2, NULL, 'Fourteen comment', '2014-05-31 11:47:45'),
(17, 2, NULL, 'Fiveteen comment', '2014-05-31 11:47:57'),
(18, 2, NULL, 'Sixteen', '2014-05-31 11:48:35'),
(19, 2, 17, 'First reply', '2014-05-31 11:49:25'),
(20, 2, 19, 'Second reply', '2014-05-31 11:49:32');

-- --------------------------------------------------------

--
-- Структура таблицы `forum_message`
--

CREATE TABLE IF NOT EXISTS `forum_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `subject_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(4096) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `subject_id` (`subject_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=49 ;

--
-- Дамп данных таблицы `forum_message`
--

INSERT INTO `forum_message` (`id`, `user_id`, `subject_id`, `parent_id`, `title`, `message`, `created_time`) VALUES
(28, 19, 16, NULL, 'tilte', 'second reply', '2014-06-04 10:38:59'),
(29, 19, 16, 28, 'title', 'third reply', '2014-06-04 10:41:03'),
(30, 19, 16, 29, 'title', 'four reply', '2014-06-04 10:48:16'),
(31, 19, 16, 30, 'five', 'reply', '2014-06-04 10:48:35'),
(32, 19, 16, 31, 'reply', 'six reply', '2014-06-04 10:48:58'),
(34, 2, 16, NULL, 'Markdown', '###Markdonw message', '2014-06-04 13:27:33'),
(35, 2, 16, 34, 'new markDown', 'A First Level Header\r\n====================\r\n\r\nA Second Level Header\r\n---------------------\r\n\r\nNow is the time for all good men to come to\r\nthe aid of their country. This is just a\r\nregular paragraph.\r\n\r\nThe quick brown fox jumped over the lazy\r\ndog''s back.\r\n\r\n### Header 3\r\n\r\n> This is a blockquote.\r\n> \r\n> This is the second paragraph in the blockquote.\r\n>\r\n> ## This is an H2 in a blockquote', '2014-06-04 13:47:27'),
(37, 2, 16, NULL, 'Markdown', 'Some of these words *are emphasized*.\r\n\r\nSome of these words _are emphasized also_.\r\n\r\nUse two asterisks for **strong emphasis**.\r\n\r\nOr, if you prefer, __use two underscores instead__.\r\n\r\n*   Candy.\r\n*   Gum.\r\n*   Booze.\r\n\r\nThis is an [example link](http://example.com/).\r\n\r\nI get 10 times more traffic from [Google][1] than from\r\n[Yahoo][2] or [MSN][3].\r\n\r\n[1]: http://google.com/        "Google"\r\n[2]: http://search.yahoo.com/  "Yahoo Search"\r\n[3]: http://search.msn.com/    "MSN Search"\r\n', '2014-06-04 14:02:52'),
(38, 2, 16, 37, 'Markdown', 'H1 header\r\n----------\r\nH2 header\r\n==========', '2014-06-04 14:55:37'),
(45, 2, 16, NULL, 'Markdown', '###Image message\r\n\r\n![alt text](http://localhost/79.png "Title")\r\n\r\nMessage', '2014-06-04 15:36:54'),
(47, 2, 16, NULL, 'Markdown', '* item\r\n* item\r\n* item\r\n\r\n1. item\r\n2. item\r\n3. item', '2014-06-04 15:42:23'),
(48, 2, 16, NULL, 'Markdown', 'Header 1\r\n--------\r\nHeader 2\r\n========\r\n\r\n###Header 3', '2014-06-04 15:46:13');

-- --------------------------------------------------------

--
-- Структура таблицы `forum_subject`
--

CREATE TABLE IF NOT EXISTS `forum_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `subject` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_closed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `subject` (`subject`(255)) USING HASH,
  KEY `user_id` (`user_id`) USING HASH
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Дамп данных таблицы `forum_subject`
--

INSERT INTO `forum_subject` (`id`, `user_id`, `subject`, `created_time`, `is_closed`) VALUES
(1, 19, 'First subject', '2014-06-02 13:02:16', 0),
(2, 19, 'Second subject', '2014-06-02 14:13:45', 0),
(3, 19, 'Third subject', '2014-06-02 14:13:54', 0),
(4, 19, 'Four subject', '2014-06-02 14:24:11', 0),
(5, 19, 'Five subject', '2014-06-02 14:24:20', 0),
(6, 19, 'Six subject', '2014-06-02 14:24:27', 0),
(8, 19, 'Nine subject', '2014-06-02 14:24:54', 0),
(9, 19, 'Ten subject', '2014-06-02 14:25:02', 0),
(10, 19, 'Eleven subject', '2014-06-02 14:25:18', 0),
(11, 19, 'Twelve subject', '2014-06-02 14:25:32', 0),
(12, 2, 'Thirteen subject', '2014-06-02 16:34:23', 0),
(13, 2, 'Fourteen', '2014-06-02 16:34:30', 0),
(14, 2, 'Fifteen subject', '2014-06-02 16:34:43', 0),
(16, 2, 'Seventeen', '2014-06-02 16:37:30', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `guestbook_message`
--

CREATE TABLE IF NOT EXISTS `guestbook_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(2056) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

--
-- Дамп данных таблицы `guestbook_message`
--

INSERT INTO `guestbook_message` (`id`, `created_time`, `name`, `title`, `message`) VALUES
(11, '2014-05-29 12:44:47', 'Serega', 'Title', 'Body'),
(12, '2014-05-29 12:44:56', 'Serega', 'Title', 'Body'),
(13, '2014-05-29 12:45:14', 'Serega', 'Title3', 'Body3'),
(14, '2014-05-29 12:45:26', 'Serega', 'Title4', 'Body4'),
(15, '2014-05-29 12:45:39', 'Serega', 'Title5', 'Body5'),
(16, '2014-05-29 12:45:59', 'Serega', 'Title6', 'Body6'),
(17, '2014-05-29 12:46:14', 'Serega', 'Title7', 'Body7'),
(18, '2014-05-29 12:46:33', 'Serega', 'Title8', 'Body8'),
(20, '2014-05-29 12:47:06', 'Serega', 'Title10', 'BOdy10'),
(28, '2014-05-29 19:21:20', 'Serega', 'Title11', 'Body11'),
(29, '2014-05-29 19:21:33', 'Serega', 'Title12', 'Body12');

-- --------------------------------------------------------

--
-- Структура таблицы `guestbook_reply`
--

CREATE TABLE IF NOT EXISTS `guestbook_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `message_id` int(11) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` varchar(2056) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `message_id` (`message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `guestbook_reply`
--

INSERT INTO `guestbook_reply` (`id`, `user_id`, `message_id`, `created_time`, `message`) VALUES
(8, 2, 14, '2014-05-29 12:48:40', 'Reply');

-- --------------------------------------------------------

--
-- Структура таблицы `user_account`
--

CREATE TABLE IF NOT EXISTS `user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `username_2` (`username`) USING HASH
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `user_account`
--

INSERT INTO `user_account` (`id`, `username`, `password`) VALUES
(2, 'admin', '$2a$08$sf231kdjDSF$sF1423klfesjO9gBLUXbTj27qgaqW2WaE/N9qwxs2'),
(19, 'demo', '$2a$08$sf231kdjDSF$sF1423klfe9YNq/29nCoj4yRu8TRWJ5.mC3ekCBWS'),
(20, 'user', '$2a$08$sf231kdjDSF$sF1423klfeZ2AWF/6PJZX6VIDqB1QVA9LP/0jNRou'),
(21, 'user1', '$2a$08$sf231kdjDSF$sF1423klfe1YNZ3LzfsaB8U2Dp9oudCsOu8pBCHf6'),
(22, 'user2', '$2a$08$sf231kdjDSF$sF1423klfe6jXLew2g2B8KTLuDqn7FPm88z2YQRB.'),
(23, 'user3', '$2a$08$sf231kdjDSF$sF1423klfec/NPSh05Nm8lChnkS57n.aK1jSZQDwe'),
(24, 'user4', '$2a$08$sf231kdjDSF$sF1423klfeNpSS0DsrJQwORcWkuGrV/seeC1BuYQ2'),
(25, 'user5', '$2a$08$sf231kdjDSF$sF1423klfedS/hvDfrKG9O9Lz1jGRARxohyCCJUiG');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comment_message`
--
ALTER TABLE `comment_message`
  ADD CONSTRAINT `comment_message_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_account` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `comment_message_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `comment_message` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `forum_message`
--
ALTER TABLE `forum_message`
  ADD CONSTRAINT `forum_message_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_account` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_message_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `forum_subject` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_message_ibfk_3` FOREIGN KEY (`parent_id`) REFERENCES `forum_message` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `forum_subject`
--
ALTER TABLE `forum_subject`
  ADD CONSTRAINT `forum_subject_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_account` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `guestbook_reply`
--
ALTER TABLE `guestbook_reply`
  ADD CONSTRAINT `guestbook_reply_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_account` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `guestbook_reply_ibfk_2` FOREIGN KEY (`message_id`) REFERENCES `guestbook_message` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
