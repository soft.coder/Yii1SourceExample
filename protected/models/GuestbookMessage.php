<?php

/**
 * This is the model class for table "guestbook_message".
 *
 * The followings are the available columns in table 'guestbook_message':
 * @property integer $id
 * @property string $created_time
 * @property string $name
 * @property string $title
 * @property string $message
 *
 * The followings are the available model relations:
 * @property GuestbookReply[] $guestbookReplies
 */
class GuestbookMessage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GuestbookMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'guestbook_message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, title, message', 'required'),
			array('name, title', 'length', 'max'=>255),
			array('message', 'length', 'max'=>2056),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, title, message', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reply' => array(self::HAS_ONE, 'GuestbookReply', 'message_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'reated_time' => 'Created Time',
			'name' => 'Name',
			'title' => 'Title',
			'message' => 'Message',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		//$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('message',$this->message,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}