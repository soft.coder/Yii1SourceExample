<?php

/**
 * This is the model class for table "user_account".
 *
 * The followings are the available columns in table 'user_account':
 * @property integer $id
 * @property string $username
 * @property string $password
 *
 * The followings are the available model relations:
 * @property GuestbookReply[] $guestbookReplies
 */
class UserAccount extends CActiveRecord
{
        public $passwordConfirm;
        public $verifyCode;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password', 'required'),
			array('username, password, passwordConfirm', 'length', 'max'=>255),
                        array('username', 'usernameMustBeUnique', 'on'=>'register'),
                        array('passwordConfirm', 'compare', 'compareAttribute'=>'password', 'on'=>'register'),
                        array('passwordConfirm', 'required', 'on'=>'register'),
                        array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'on'=>'register'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password', 'safe', 'on'=>'search'),
		);
	}
        public function register($username, $password, $passwordConfirm, $verifyCode)
        {
            $this->scenario = 'register';
            $this->verifyCode = $verifyCode;

            $this->username = $username;
            if($password){
                $this->password = crypt($password, '$2a$08$sf231kdjDSF$sF1423klfesjO9gBLUXbTj27qgaqW2WaE/N9qwxs2');
                $this->passwordConfirm = crypt($passwordConfirm, '$2a$08$sf231kdjDSF$sF1423klfesjO9gBLUXbTj27qgaqW2WaE/N9qwxs2');
            }
            if($this->save())
                return true;
            else{
                $this->unsetAttributes(array('password', 'passwordConfirm'));
                return false;
            }
        }
        public function usernameMustBeUnique($attribute, $params)
        {
            $count = $this->countByAttributes(array('username'=> $this->username));
            if($count > 0)
                $this->addError('username', "User with such username is already exist");
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'guestbookReplies' => array(self::HAS_MANY, 'GuestbookReply', 'user_id'),
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
                        'passwordConfirm' => 'Password confirm'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}