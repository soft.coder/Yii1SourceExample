<?php

class UserController extends Controller
{
        public $defaultAction='list';
        
	public function actionList()
	{
            $dataProvider =  new CActiveDataProvider('UserAccount', array(
                'criteria'=>array(
                    'condition'=>"username!='admin'"
                )
            ));
            $this->render('index', array(
                'dataProvider'=>$dataProvider
            ));
	}
        public function actionRegister()
        {
            $user = new UserAccount;
            $user->scenario='register';
            if(isset($_POST['UserAccount']))
            {
                $username = $_POST['UserAccount']['username'];
                $password = $_POST['UserAccount']['password'];
                $passwordConfirm = $_POST['UserAccount']['passwordConfirm'];
                $verfiyCode = $_POST['UserAccount']['verifyCode'];
                if($user->register($username, $password, $passwordConfirm, $verfiyCode)){
                    $identity = new UserIdentity($username, $password);
                    if($identity->authenticate())
                        Yii::app()->user->login($identity);
                    
                    $this->redirect(Yii::app()->homeUrl);
                }
                    
            }
            $this->render('register', array(
                'model'=> $user
            ));
        }
        public function actionDelete($id)
        {
            $id = (int)$id;
            $user = UserAccount::model()->findByPk($id);
            $user->delete();
            $this->redirect(array('user/list'));
        }

	public function filters()
	{
		return array(
			'accessControl'
		);
	}
        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions'=>array('register', 'captcha'),
                    'users'=>array('?')
                ),
                array(
                    'allow',
                    'actions'=>array('list', 'delete'),
                    'users'=>array('admin')
                ),
                array(
                    'deny',
                    'users'=>array('*')
                )
            );
        }
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
                                'testLimit'=>2
			)
		);
	}
}