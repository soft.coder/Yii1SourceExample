<?php

class ViewController extends Controller
{
	public function actionAdd()
	{
            $comment = new CommentMessage;
            $comment->scenario = 'add';
            if(isset($_POST['CommentMessage']))
            {
                $comment->attributes = $_POST['CommentMessage'];
                $comment->user_id = Yii::app()->user->id;
                if($comment->save())
                    $this->redirect (array('/comment/view/index'));
            }
            $this->render('add', array(
                'model'=>$comment
            ));
	}

	public function actionDelete($id)
	{
            $comment = CommentMessage::model()->findByPk((int)$id);
            $comment->scenario='delete';
            $comment->unsetAttributes(array('message'));
            $comment->save();
            $this->redirect(array('/comment/view/index'));
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionReply($id)
	{
            $comment = new CommentMessage;
            $comment->scenario = 'reply';
            if(isset($_POST['CommentMessage']))
            {
                $comment->attributes = $_POST['CommentMessage'];
                $comment->user_id = Yii::app()->user->id;
                $comment->parent_id = $id;
                if($comment->save())
                    $this->redirect (array('/comment/view/index'));
            }
            $this->render('reply', array(
                'model'=>$comment,
                'repliedComment'=>  CommentMessage::model()->findByPk((int)$id)
            ));
	}

	public function filters()
	{
		return array(
			'accessControl'
		);
	}
        public function accessRules(){
            return array(
                array(
                    'allow',
                    'actions'=>array('index'),
                    'users'=>array('*')                    
                ),
                array(
                    'allow',
                    'actions'=>array('add', 'reply'),
                    'users'=>array('@')                    
                ),
                array(
                    'allow',
                    'actions'=>array('delete'),
                    'users'=>array('admin')
                ),
                array(
                    'deny',
                    'users'=>array('*')
                )
                
            );
        }
        /*
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}