<?php

/**
 * This is the model class for table "comment_message".
 *
 * The followings are the available columns in table 'comment_message':
 * @property integer $id
 * @property integer $user_id
 * @property integer $parent_id
 * @property string $message
 * @property string $time_added
 *
 * The followings are the available model relations:
 * @property UserAccount $user
 * @property CommentMessage $parent
 * @property CommentMessage[] $commentMessages
 */
class CommentMessage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CommentMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comment_message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message', 'required', 'except'=>'delete'),
			array('user_id, parent_id', 'numerical', 'integerOnly'=>true),
			array('message', 'length', 'max'=>2056),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, parent_id, message, time_added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
			'parent' => array(self::BELONGS_TO, 'CommentMessage', 'parent_id'),
			'replies' => array(self::HAS_MANY, 'CommentMessage', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'parent_id' => 'Parent',
			'message' => 'Message',
			'time_added' => 'Time Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('time_added',$this->time_added,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}