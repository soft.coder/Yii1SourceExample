<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<?php echo $this->renderPartial('_comment', $data); ?>
<p>
    <?php echo CHtml::link('Reply', array('view/reply', 'id'=>$data->id)); ?>
    <?php if(Yii::app()->user->name==='admin'): ?>
        <?php echo CHtml::link('Delete', array('view/delete', 'id'=>$data->id), array(
            'confirm'=>'Do you realy want to delete this comment'
        )); ?>
    <?php endif; ?>
</p>
<hr />
<div style='margin-left: 20px'>
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => new CActiveDataProvider('CommentMessage', array(
        'criteria'=>array(
            'condition'=>"parent_id={$data->id}",
            'order'=>'time_added DESC',
            'with'=>'user'
        )
    )),
    'itemView'=>'_view',
    'emptyText'=>'',
    'enablePagination'=>false,
    'summaryText'=>''
));
?>
</div>
