<?php
/* @var $this ViewController */

$this->breadcrumbs=array(
	'Comments',
);
?>
<h1>Comments</h1>
<p class="comment-add"><?php echo CHtml::link('Add comment', array('view/add')); ?></p>
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => new CActiveDataProvider('CommentMessage', array(
        'criteria'=>array(
            'condition'=>'parent_id IS NULL',
            'order'=>'time_added DESC',
            'with'=>'user'
        )
    )),
    'itemView'=>'_view'
));
?>