<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$this->pageTitle=Yii::app()->name . ' - Comments';
$this->breadcrumbs=array(
	'Comments'=>array('/comment/view/index'),
        'Add comment'
);

?>

<h1>Add Comment</h1>
<div class='form'>
<?php $form=$this->beginWidget('CActiveForm', array(
    'enableClientValidation'=> TRUE,
    //'focus'=>array($model, 'message')
));?>
    
    <?php echo $form->errorSummary($model); ?>
    <div class='row'>
        <?php echo $form->labelEx($model, 'message'); ?>
        <?php echo $form->textArea($model, 'message'); ?>
        <?php echo $form->error($model, 'message'); ?>
    </div>
    <?php echo CHtml::submitButton('Add'); ?>
<?php $this->endWidget(); ?>
</div>