<?php
/* @var $this ViewController */

$this->pageTitle=Yii::app()->name . ' - Comments';
$this->breadcrumbs=array(
	'Comments'=>array('/comment/view/index'),
        'Reply comment'
);
?>
<h1>Reply comment</h1>
<?php $this->renderPartial('_comment', $repliedComment) ?>
<div class='form'>
<?php $form=$this->beginWidget('CActiveForm', array(
    'enableClientValidation'=> TRUE,
    //'focus'=>array($model, 'message')
));?>
    
    <?php echo $form->errorSummary($model); ?>
    <div class='row'>
        <?php echo $form->labelEx($model, 'message'); ?>
        <?php echo $form->textArea($model, 'message'); ?>
        <?php echo $form->error($model, 'message'); ?>
    </div>
    <?php echo CHtml::submitButton('Add'); ?>
<?php $this->endWidget(); ?>
</div>
