<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class='comment'>
    <p class="comment-title">
        Comment of&nbsp;<strong><?php echo CHtml::encode($data->user->username); ?> </strong>
        &nbsp;in <i><?php echo CHtml::encode($data->time_added); ?></i>
    </p>
    <?php if(!is_null($data->message)): ?>
        <p class="comment-message"><?php echo CHtml::encode($data->message); ?></p>
    <?php else: ?>
        <p class="comment-message-deleted" style='color: darkred'>[deleted]</p>
    <?php endif; ?>
</div>