<?php

class ViewController extends Controller
{
	public function actionIndex()
	{
            $dataProvider = new CActiveDataProvider('GuestBookMessage', array(
               'criteria' => array(
                   'order' => 't.created_time DESC',
                   'with' => array('reply')
               )
            ));
            $this->render('index', array(
                'dataProvider' => $dataProvider
            ));
	}
        public function actionReply($message_id)
        {
            $id = (int)$message_id;
            $replyModel = new GuestbookReply;
            if(isset($_POST["GuestbookReply"]))
            {
                $replyModel->user_id = Yii::app()->user->id;
                $replyModel->message_id = $id;
                $replyModel->message = $_POST["GuestbookReply"]["message"];
                if($replyModel->save())
                    $this->redirect(array('view/index'));
            }
            $guestBookMessage = GuestbookMessage::model()->findByPk($id);
            
            $this->render('reply', array(
                'data'=> $guestBookMessage,
                'replyModel' => $replyModel
            ));
        }
        public function actionAdd()
        {
            $addModel = new GuestbookMessage;
            if(isset($_POST['GuestbookMessage']))
            {
                $addModel->attributes = $_POST['GuestbookMessage'];
                if($addModel->save())
                    $this->redirect(array('view/index'));
            }
            $this->render('add', array(
                'addModel'=> $addModel
            ));
        }
        public function actionDelete($message_id)
        {
            $message_id = (int)$message_id;
            $gbMessage = GuestbookMessage::model()->findByPk($message_id);
            if($gbMessage->delete())
                $this->redirect(array('view/index'));
        }
        public function actionDeleteReply($reply_id)
        {
            $reply_id = (int)$reply_id;
            $gbReply = GuestbookReply::model()->findByPk($reply_id);
            if($gbReply->delete())
                $this->redirect(array('view/index'));
        }
	// Uncomment the following methods and override them if needed
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'accessControl',
		);
	}
        public function accessRules() {
            return array(
                array(
                    'allow', 
                    'actions'=>array('index','add', 'captcha'),
                    'users'=>array('*')
                ),
                array(
                    'allow',
                    'actions'=>array('reply', 'delete', 'deleteReply'),
                    'users'=>array('admin')
                ),
                array(
                    'deny',
                    'users'=>array('*')
                )
            );
        }
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'testLimit'=>1,
			),
		);
	}
}