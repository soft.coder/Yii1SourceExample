<?php
/* @var $this ViewController */

$this->pageTitle=Yii::app()->name . ' - Guestbook';
$this->breadcrumbs=array(
	'Guestbook',
);
?>
<h1>Guestbook</h1>

<div class='add'>
    <p><?php echo CHtml::link('Add message', array('view/add')); ?></p>
</div>

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView'=>'_view'
));
?>