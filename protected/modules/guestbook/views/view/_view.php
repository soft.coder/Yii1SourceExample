<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="gb">
    <?php echo $this->renderPartial('_message', $data); ?>
    <?php if(Yii::app()->user->name==='admin'): ?>
    <div class='buttons'><p>
        <?php echo CHtml::link('Delete', array('view/delete', 'message_id'=>$data->id), array('confirm'=>'Are you sure you want to delete this message?')); ?>
        <?php if(!$data->reply): ?>
            <?php echo CHtml::link('Reply', array('view/reply', 'message_id'=>$data->id)); ?>
        <?php endif; ?>
    </div></p>
    <?php endif; ?>
    
    <?php if($data->reply): ?>
        <?php echo $this->renderPartial('_reply', $data); ?>
    <?php endif; ?>
    <hr />
</div>
