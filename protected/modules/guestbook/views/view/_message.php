<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class='message'>
    <div class='title'>
        <p>
            <strong><?php echo CHtml::encode($data->title); ?></strong>
            by&nbsp;<i><?php echo CHtml::encode($data->name); ?> </i>
            &nbsp;in <i><?php echo CHtml::encode($data->created_time); ?></i>
        </p>
    </div>
    <div class='message'>
        <p><?php echo CHtml::encode($data->message); ?></p>
    </div>

</div>
