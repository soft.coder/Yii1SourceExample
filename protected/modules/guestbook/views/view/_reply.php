<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class='reply' style='margin-left: 20px;'>
    <div class='info'>
        <p>
            <?php if($data->reply->user): ?>
            <i><?php echo CHtml::encode($data->reply->user->username); ?> </i>
            <?php else: ?>
            <strong>[user deleted]</strong>
            <?php endif; ?>
            &nbsp;in <i><?php echo CHtml::encode($data->reply->created_time); ?></i>
        </p>
    </div>
    <div class='message'>
        <p><?php echo CHtml::encode($data->reply->message); ?></p>
    </div>            
    <?php if(Yii::app()->user->name==='admin'): ?>
        <p><?php echo CHtml::link('Delete', array('view/deleteReply', 'reply_id'=>$data->reply->id)); ?></p>
    <?php endif; ?>
</div>