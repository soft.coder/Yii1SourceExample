<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$this->pageTitle=Yii::app()->name . ' - Guestbook';
$this->breadcrumbs=array(
	'Guestbook'=>array('view/index'),
        'Add message'
);

?>

<h1>Guestbook</h1>
<div class='form'>
<?php $form=$this->beginWidget('CActiveForm', array(
    'enableClientValidation'=> TRUE,
    //'focus'=>Yii::app()->user->isGuest ? array($addModel, 'name') : array($addModel, 'title')
));?>
    
    <?php echo $form->errorSummary($addModel); ?>
    <div class='row'>
        <?php echo $form->labelEx($addModel, 'name'); ?>
        <?php echo $form->textField($addModel, 'name', array(
            'value'=>Yii::app()->user->isGuest ? '' : Yii::app()->user->name)); ?>
        <?php echo $form->error($addModel, 'name'); ?>
    </div>
    <div class='row'>
        <?php echo $form->labelEx($addModel, 'title'); ?>
        <?php echo $form->textField($addModel, 'title'); ?>
        <?php echo $form->error($addModel, 'title'); ?>
    </div>
    <div class='row'>
        <?php echo $form->labelEx($addModel, 'message'); ?>
        <?php echo $form->textArea($addModel, 'message'); ?>
        <?php echo $form->error($addModel, 'message'); ?>
    </div>
    <?php if(CCaptcha::checkRequirements() && Yii::app()->user->isGuest): ?>
    <div class='row'>
        <div>
            <?php $this->widget('CCaptcha', array(
                'showRefreshButton'=>false
            ));?>
        </div>
        <?php echo $form->labelEx($addModel, 'verifyCode'); ?>
        <?php echo $form->textField($addModel, 'verifyCode'); ?>
        <?php echo $form->error($addModel, 'verifyCode'); ?>
    </div>
    <?php endif; ?>
    <?php echo CHtml::submitButton('Add'); ?>
<?php $this->endWidget(); ?>
</div>