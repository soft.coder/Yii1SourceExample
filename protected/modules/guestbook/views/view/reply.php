<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->pageTitle=Yii::app()->name . ' - Guestbook';
$this->breadcrumbs=array(
	'Guestbook'=>array('view/index'),
        'Reply message'
);
?>
<h1>Guestbook</h1>
<?php echo $this->renderPartial('_message', $data); ?>
<div class='form'>
<?php $form=$this->beginWidget('CActiveForm', array(
    'enableClientValidation'=> TRUE,
    //'focus'=>array($replyModel, 'message')
));?>
    
    <?php echo $form->errorSummary($replyModel); ?>
    <div class='row'>
        <?php echo $form->labelEx($replyModel, 'message'); ?>
        <?php echo $form->textArea($replyModel, 'message'); ?>
        <?php echo $form->error($replyModel, 'message'); ?>
    </div>
    <?php echo CHtml::submitButton('Reply'); ?>
<?php $this->endWidget(); ?>
</div>