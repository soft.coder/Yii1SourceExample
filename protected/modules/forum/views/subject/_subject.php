<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<p>
    <?php echo Chtml::link($data->subject, array('message/index', 'id'=>$data->id)); ?>
    <?php if($data->is_closed==true): ?>
        <span class="forum-deleted">[closed]</span>
    <?php endif; ?> in
    <i><?php echo $data->created_time ?></i> with 
    <i>(<?php echo $data->countMessages ?>)</i> messages by
    <strong><?php echo $data->user->username ?></strong>
    <?php if(Yii::app()->user->name==='admin'): ?>
        <?php if($data->is_closed==false): ?>
            <?php echo CHtml::link('Close', array('subject/close', 'id'=>$data->id), array(
                'confirm'=>'Do you realy want to close this subject of forum'
            )); ?>
        <?php else: ?>
            <?php echo CHtml::link('Open', array('subject/open', 'id'=>$data->id), array(
                'confirm'=>'Do you realy want to open this subject of forum'
            )); ?>
        <?php endif; ?>
        <?php echo CHtml::link('Delete', array('subject/delete', 'id'=>$data->id), array(
        'confirm'=>'Do you realy want to delete this subject of forum'
        )); ?>
    
    <?php endif; ?>
</p>
<hr />