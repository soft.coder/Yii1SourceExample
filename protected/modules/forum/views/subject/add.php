<?php
/* @var $this SubjectController */

$this->breadcrumbs=array(
	'Forum subjects'=>array('/subject'),
	'Add subject',
);
?>
<h1>Add forum subject</h1>

<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
    'enableClientValidation'=>true
)); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'subject'); ?>
        <?php echo $form->textField($model, 'subject'); ?>
        <?php echo $form->error($model, 'subject'); ?>
    </div>
    <?php echo CHtml::submitButton('Add subject'); ?>
<?php $this->endWidget(); ?>
</div> 