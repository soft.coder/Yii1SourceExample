<?php
/* @var $this SubjectController */
$this->pageTitle = Yii::app()->name . ' - Forum subjects';
$this->breadcrumbs=array(
	'Forum subjects',
);
?>
<h1>Forum subjects</h1>

<p><?php echo CHtml::link('Add subject', array('subject/add')) ?></p>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_subject'
)); ?>