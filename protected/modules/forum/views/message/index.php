<?php
/* @var $this MessageController */

$this->breadcrumbs=array(
	'Forum'=>array('subject/index'),
        'Messages'
);
?>
<h1>
    <?php echo $this->subject->subject ?>
    <?php if($this->subject->is_closed==true): ?>
    - <span class="forum-deleted">[closed]</span>
    <?php endif; ?>
</h1>

<?php if($this->subject->is_closed == false): ?>
    <p><?php echo CHtml::link('Add message', array('message/add', 'id'=>$this->subject->id)) ?></p>
<?php endif; ?>
    
<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_view'
)); ?>


