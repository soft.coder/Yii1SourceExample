<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs=array(
	'Forum'=>array('subject/index'),
        'Messages'=>array('message/index', 'id'=>$chainMessages[0]->subject_id),
        'Branch'
);
?>
<h1>Branch of messages</h1>
<?php for($i=0, $count=count($chainMessages); $i<$count; $i++): ?>
    <?php $this->renderPartial('_message', array(
        'message'=>$chainMessages[$i],
        'showQuotation'=>false,
        'showMessageButtons'=> ($i==($count-1)) ? true : false)); ?>
<?php endfor; ?>