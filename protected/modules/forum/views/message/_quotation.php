<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="forum-quote">
    <?php if(isset($message->user)): ?>
        <strong><?php echo CHtml::encode($message->user->username); ?></strong>
    <?php else: ?>
        <span class="forum-deleted">[user deleted]</span>
    <?php endif; ?>
        <i><?php echo $message->created_time; ?>:</i> 
    <br />
    <?php $markDown = new CMarkdownParser(); ?>
    <?php echo $markDown->transform($message->message) ?>
</div>