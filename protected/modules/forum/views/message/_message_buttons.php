<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<p>
<?php if($this->subject->is_closed==false): ?>
    <?php echo CHtml::link('Reply', array('message/reply', 'id'=>$message->id)); ?>
<?php endif; ?>
<?php if(Yii::app()->user->name==='admin'): ?>
    <?php echo CHtml::link('Delete', array('message/delete', 'id'=>$message->id), array(
        'confirm'=>'Do you realy want to delete this comment'
    )) ?>
<?php endif; ?>
</p>