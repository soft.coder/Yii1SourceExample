<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$showQuotation = isset($showQuotation) ? $showQuotation : false;
$showMessageButtons = isset($showMessageButtons) ? $showMessageButtons : false;
?>

<p>
    <?php echo $message->title ?>
    (<i><?php echo $message->created_time ?></i>) by 
    <?php if(isset($message->user)): ?>
        <strong><?php echo $message->user->username; ?></strong>
    <?php else: ?>
        <span class="forum-deleted">[user deleted]</span>
    <?php endif; ?>
    <?php echo CHtml::link('#', "#$message->id", array('name'=>$message->id)); ?>
    <?php if(isset($message->parent)): ?>
        <?php echo CHtml::link('branch', array('message/branch', 'id'=>$message->id)); ?>
    <?php endif; ?>
</p>
<?php if(isset($message->parent) && $showQuotation): ?>
    <?php $this->renderPartial('_quotation', array(
        'message'=>$message->parent
    )); ?>
<?php endif; ?>

<?php $markDown = new CMarkdownParser(); ?>
<?php echo $markDown->transform($message->message) ?>
<?php if($showMessageButtons): ?>
    <?php $this->renderPartial('_message_buttons', array(
        'message'=>$message
    )); ?>
<?php endif; ?>
<hr />