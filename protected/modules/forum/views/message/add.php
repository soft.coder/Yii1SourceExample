<?php
/* @var $this MessageController */

$this->breadcrumbs=array(
        'Forum'=>array('subject/index'),
	'Messages'=>array('message/index', 'id'=>$this->subject->id),
	'Add',
);
?>
<h1>Add message</h1>

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'enableClientValidation'=>true
    ));?>
    <?php echo $form->errorSummary($model, 'title') ?>
    <div class='row'>
        <?php echo $form->labelEx($model, 'title') ?>
        <?php echo $form->textField($model, 'title') ?>
        <?php echo $form->error($model, 'title') ?>
    </div>
    <div class='row'>
        <?php echo $form->labelEx($model, 'message') ?>
        <?php echo $form->textArea($model, 'message', array(
            'col'
        )) ?>
        <?php echo $form->error($model, 'message') ?>
    </div>
    <?php echo CHtml::submitButton('Add message') ?>
    <?php $this->endWidget() ?>
</div>