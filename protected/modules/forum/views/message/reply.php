<?php
/* @var $this MessageController */

$this->breadcrumbs=array(
	'Forum'=>array('subject/index'),
	'Messages'=>array('message/index', 'id'=>$message->subject_id),
        'Reply'
);
?>
<h1>Reply message</h1>
<?php $this->renderPartial('_message', array('message'=>$message)) ?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'enableClientValidation'=>true
    ));?>
    <?php echo $form->errorSummary($model, 'title') ?>
    <div class='row'>
        <?php echo $form->labelEx($model, 'title') ?>
        <?php echo $form->textField($model, 'title') ?>
        <?php echo $form->error($model, 'title') ?>
    </div>
    <div class='row'>
        <?php echo $form->labelEx($model, 'message') ?>
        <?php echo $form->textArea($model, 'message') ?>
        <?php echo $form->error($model, 'message') ?>
    </div>
    <?php echo CHtml::submitButton('Reply message') ?>
    <?php $this->endWidget() ?>
</div>