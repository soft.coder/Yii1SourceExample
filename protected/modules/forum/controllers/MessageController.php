<?php

class MessageController extends Controller
{
        public $subject;
	public function actionAdd($id)
	{
            $model = new ForumMessage;
            $model->scenario='add';
            if(isset($_POST['ForumMessage']))
            {
                $model->attributes = $_POST['ForumMessage'];
                $model->user_id = Yii::app()->user->id;
                $model->subject_id = $id;
                if($model->save())
                    $this->redirect(array('message/index', 'id'=>$id));
            }
            $this->subject = ForumSubject::model()->findByPk((int)$id);
            $this->render('add', array(
                'model'=>$model,
            ));
	}

	public function actionDelete($id)
	{
            $message = ForumMessage::model()->findByPk($id);
            if(isset($message))
                $message->delete();
            $this->redirect(Yii::app()->request->urlReferrer);
	}

	public function actionIndex($id)
	{
            $dataProvider = new CActiveDataProvider('ForumMessage', array(
                'criteria'=>array(
                    'condition'=>"t.subject_id=$id",
                    'order'=>'t.created_time ASC',
                    'with'=>array('user', 'parent', 'parentUser', 'subject')
                )
            ));
            $this->subject = ForumSubject::model()->findByPk((int)$id);
            $this->render('index', array(
                'dataProvider'=>$dataProvider,
            ));
	}

	public function actionReply($id)
	{
            $message = ForumMessage::model()->findByPk($id);
            $model = new ForumMessage;
            $model->scenario='reply';
            if(isset($_POST['ForumMessage']))
            {
                $model->attributes = $_POST['ForumMessage'];
                $model->user_id = Yii::app()->user->id;
                $model->subject_id = $message->subject_id;
                $model->parent_id = $id;
                if($model->save())
                    $this->redirect(array('message/index', 'id'=>$message->subject_id));
            }
            
            $this->render('reply', array(
                'model'=>$model,
                'message'=>$message
            ));
	}
        public function actionBranch($id)
        {
            $messageLeaf = ForumMessage::model()->findByPk($id);
            $this->subject = ForumSubject::model()->findByPk($messageLeaf->subject_id);
            $parentMessage = $messageLeaf;
            $chainMessages = array();
            while(isset($parentMessage)){
                array_unshift($chainMessages, $parentMessage);
                $parentMessage = $parentMessage->parent;
            }
            $this->render('branch',array(
                'chainMessages'=>$chainMessages 
            ));
        }
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'accessControl',
		);
	}
        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions'=>array('index', 'branch'),
                    'users'=>array('*')
                ),
                array(
                    'allow',
                    'actions'=>array('add', 'reply'),
                    'users'=>array('@')
                ),
                array(
                    'allow',
                    'actions'=>array('delete'),
                    'users'=>array('admin')
                ),
                array(
                    'deny',
                    'users'=>array('*')
                )
            );
        }
        /*
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}