<?php

class SubjectController extends Controller
{
	public function actionAdd()
	{
            $model = new ForumSubject;
            $model->scenario = 'add';
            if(isset($_POST['ForumSubject']))
            {
                $model->subject = $_POST['ForumSubject']['subject'];
                $model->user_id = Yii::app()->user->id;
                if($model->save())
                    $this->redirect(array('subject/index'));
            }
            $this->render('add', array(
                'model'=>$model
            ));
	}

	public function actionClose($id)
	{
            $subject = ForumSubject::model()->findByPk($id);
            if(isset($subject)){
                $subject->is_closed = 1;
                $subject->save();
            }
            $this->redirect(Yii::app()->request->urlReferrer);
	}
        public function actionOpen($id)
        {
            $subject = ForumSubject::model()->findByPk($id);
            if(isset($subject)){
                $subject->is_closed = 0;
                $subject->save();
            }
            $this->redirect(Yii::app()->request->urlReferrer);
        }

	public function actionDelete($id)
	{
            $subject = ForumSubject::model()->findByPk($id);
            if(isset($subject))
                $subject->delete();
            
            $this->redirect(Yii::app()->request->urlReferrer);
	}

	public function actionIndex()
	{
            $dataProvider = new CActiveDataProvider('ForumSubject', array(
                'criteria'=>array(
                    'order'=>'t.created_time DESC',
                    'with'=>array('messages', 'countMessages', 'user')
                )
            ));
            $this->render('index', array(
                'dataProvider'=>$dataProvider
            ));
	}

	public function filters()
	{
		return array(
			'accessControl'
		);
	}
        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions'=>array('index'),
                    'users'=>array('*')
                ),
                array(
                    'allow',
                    'actions'=>array('add'),
                    'users'=>array('@')
                ),
                array(
                    'allow',
                    'actions'=>array('close', 'delete', 'open'),
                    'users'=>array('admin')
                ),
                array(
                    'deny',
                    'users'=>array('*')
                )
            );
        }
        /*
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}