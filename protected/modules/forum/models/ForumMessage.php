<?php

/**
 * This is the model class for table "forum_message".
 *
 * The followings are the available columns in table 'forum_message':
 * @property integer $id
 * @property integer $user_id
 * @property integer $subject_id
 * @property integer $parent_id
 * @property string $title
 * @property string $message
 * @property string $created_time
 *
 * The followings are the available model relations:
 * @property UserAccount $user
 * @property ForumSubject $subject
 * @property ForumMessage $parent
 * @property ForumMessage[] $forumMessages
 */
class ForumMessage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ForumMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'forum_message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject_id', 'required'),
                        array('title, message', 'required', 'on'=>'add, reply'),
			array('user_id, subject_id, parent_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>512),
			array('message', 'length', 'max'=>4096),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, subject_id, parent_id, title, message, created_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
			'subject' => array(self::BELONGS_TO, 'ForumSubject', 'subject_id'),
			'parent' => array(self::BELONGS_TO, 'ForumMessage', 'parent_id'),
			'childs' => array(self::HAS_MANY, 'ForumMessage', 'parent_id'),
                        'parentUser' => array(
                            self::HAS_ONE, 'UserAccount', array('user_id'=>'id'), 'through'=>'parent'
                        )
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'subject_id' => 'Subject',
			'parent_id' => 'Parent',
			'title' => 'Title',
			'message' => 'Message',
			'created_time' => 'Created Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('subject_id',$this->subject_id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('created_time',$this->created_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}