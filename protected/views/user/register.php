<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Register';
$this->breadcrumbs=array(
	'Register',
);
?>

<h1>Register</h1>

<p>Please fill out the following form with your register credentials:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'enableClientValidation'=>true,
        //'focus'=>array($model,'username'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
        <?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'passwordConfirm'); ?>
		<?php echo $form->passwordField($model,'passwordConfirm'); ?>
		<?php echo $form->error($model,'passwordConfirm'); ?>
	</div>
        <?php if(CCaptcha::checkRequirements()): ?>
        <div class="row">
            <div>
            <?php $this->widget('CCaptcha', array(
                'showRefreshButton'=>false, 
                'clickableImage'=>true) ); ?>
            </div>
            <?php echo $form->labelEx($model, 'verifyCode') ?>
            <?php echo $form->textField($model, 'verifyCode'); ?>
            
            <?php echo $form->error($model,'verifyCode'); ?>
        </div>
        <?php endif; ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Register'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
