<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'columns'=>array(
        'id',
        'username',
        array(
            'class'=>'CButtonColumn',
            'buttons'=>array(
                'update'=>array(
                    'visible'=>'false'
                ),
                'view'=>array(
                    'visible'=>'false'
                )
            )
        )
    )
));
